import Device from "./Device";
import Outage from "./Outage";

interface SiteOutage extends Device, Outage {}

export default SiteOutage;