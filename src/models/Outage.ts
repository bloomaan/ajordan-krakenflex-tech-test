interface Outage {
    id: string;
    begin: string;
    end: string;
  }

  export default Outage;