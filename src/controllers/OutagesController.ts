import { Request, Response } from "express";
import { Service } from "typedi";
// Services
import SiteOutageService from "../services/SiteOutageService";
// Exceptions
import NotFound from "../repositories/exceptions/NotFound";

@Service()
class OutagesController {
  constructor(
    private readonly siteOutageService: SiteOutageService
  ) { }
  async updateOutagesForSiteAndHandleErrors(req: Request, res:Response) {
    try{
      await this.updateOutagesForSite(req, res);
    } catch (e) {
      if (e instanceof NotFound) {
        console.error(e.name,e.message, e.stack);
        res.status(404);
        res.send(e.message);
        return;
      }
      res.status(500);
      if (e instanceof Error) {
        console.error(e.name,e.message, e.stack);
        res.send(e.message);
      }
      return;
    }
  }
  private async updateOutagesForSite(req: Request, res: Response) {
    // Validate Inputs
    const dateQueryParam: any = req.query.date;
    if (typeof dateQueryParam !== "string") {
      res.status(400);
      res.send("Query parameter date is required and must be a valid date string");
      return;
    }
    const dateToFilterBy = new Date(dateQueryParam);
    if (undefined === dateToFilterBy) {
      res.status(400);
      res.send("Query parameter date must be a valid date format");
      return;
    }
    const siteId = req.params.siteId;
    if (typeof siteId !== "string") {
      res.status(400);
      res.send("The siteId path parameter is required");
      return;
    }

    const siteOutages = await this.siteOutageService.getAllByIdAfterDate(siteId, dateToFilterBy);
    await this.siteOutageService.save(siteId, siteOutages);

    return res.json(
      {
        status: "The following data was sucessfully sent to the site-outages endpoint",
        data: siteOutages
      }
    );
  }
}

export default OutagesController;