class NotFound extends Error {
    constructor(message: string) {
        const fullMessage = `NotFound: ${message}`;
        super(fullMessage);
    }
}

export default NotFound;