import { Service, Inject } from "typedi";
import {AxiosError, AxiosInstance, AxiosResponse} from "axios";
// Models
import Outage from "../models/Outage";
import SiteInfo from "../models/SiteInfo";
import SiteOutage from "../models/SiteOutage";
// Exceptions
import NotFound from "./exceptions/NotFound";
import PermissionRequired from "./exceptions/PermissionRequired";
import RateLimit from "./exceptions/RateLimit";
import IncorrectInput from "./exceptions/IncorrectInput";

@Service()
class KrakenFlexAPIRepository {
  constructor(@Inject("kf-api-client") private readonly client: AxiosInstance) { }

  async getAllOutages(): Promise<Outage[]> {
    let response;
    try {
      response = await this.client.get("/outages");
    } catch (e) {
      if (e instanceof AxiosError) {
        this.handleGenericApiErrors(e);
      }
      throw e;
    }
    return response.data;
  }

  async getSiteInfoById(siteId: string): Promise<SiteInfo> {
    let response: AxiosResponse;
    try {
      response = await this.client.get(`/site-info/${encodeURIComponent(siteId)}`);
    } catch (e) {
      if (e instanceof AxiosError) {
        if (404 === e.response?.status) {
          throw new NotFound(`site with id '${siteId}'`);
        }
        this.handleGenericApiErrors(e);
      }
      throw e;
    }
    return response.data;
  }

  async saveSiteOutage(siteId: string, siteOutages: SiteOutage[]): Promise<void> {
    const url = `/site-outages/${encodeURIComponent(siteId)}`;
    try {
      await this.client.post(url, siteOutages);
    } catch (e) {
      if (e instanceof AxiosError) {
        if (404 === e.response?.status) {
          throw new NotFound(`site with id '${siteId}'`);
        }
        if (400 === e.response?.status) {
          throw new IncorrectInput(`Incorrect Data Format sent to POST ${url}`);
        }
        this.handleGenericApiErrors(e);
      }
      throw e;
    }
  }

  private handleGenericApiErrors(e: AxiosError) {
    console.error(e.message, e.stack);
    if (403 === e.response?.status) {
      throw new PermissionRequired("The Kraken Flex API requires a valid api key");
    }
    if (429 === e.response?.status) {
      throw new RateLimit("Rate Limit hit for the Kraken Flex API")
    }
  }
}

export default KrakenFlexAPIRepository;