import "reflect-metadata";

import express from "express";
import Container from "typedi";
import axios from "axios";
// Controllers
import OutagesController from "./controllers/OutagesController";
// Config
import kfapiConfig from "./config/kraken-flex-api.json";

const main = async () => {
  const app = express();
  // Initialise environment
  Container.set(
    "kf-api-client",
    axios.create(
      {
        "baseURL": kfapiConfig.baseURL,
        "headers": {
          "x-api-key": kfapiConfig.apiKey
        }
      }
    )
  );

  // Define Endpoints
  const outagesController = Container.get(OutagesController);

  app.post("/outages/:siteId", async (req, res) => {
      await outagesController.updateOutagesForSiteAndHandleErrors(req, res);
  });

  app.listen(3000, () => {
    console.log("Server started");
  });
}

main().catch(err => {
  console.error(err);
});