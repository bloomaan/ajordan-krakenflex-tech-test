import { Service } from "typedi";
import KFAPIRepository from "../repositories/KrakenFlexAPIRepository";
import SiteOutage from "../models/SiteOutage";

@Service()
class SiteOutageService {
  constructor(private readonly kfapiRepository: KFAPIRepository) { }

  async getAllByIdAfterDate(siteId: string, dateToFilterFrom: Date): Promise<SiteOutage[]> {
    const outages = await this.kfapiRepository.getAllOutages();
    const siteInfo = await this.kfapiRepository.getSiteInfoById(siteId);
    let deviceNameById: {[index: string]: string} = {};
    for (let device of siteInfo.devices) {
      deviceNameById[device.id] = device.name;
    }
    let siteOutages: SiteOutage[] = [];
    for (let outage of outages) {
      let outageBegan = new Date(outage.begin);
      if ((outageBegan >= dateToFilterFrom) && deviceNameById.hasOwnProperty(outage.id)) {
        siteOutages.push(
          {
            "id": outage.id,
            "name": deviceNameById[outage.id],
            "begin": outage.begin,
            "end": outage.end
          }
        );
      }
    }

    return siteOutages;
  }

  async save(siteId: string, siteOutages: SiteOutage[]): Promise<void> {
    await this.kfapiRepository.saveSiteOutage(siteId, siteOutages);
  }
}

export default SiteOutageService;