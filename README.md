# AJordan KrakenFlex Tech Test

Project layout based on [this article](https://levelup.gitconnected.com/dependency-injection-in-typescript-2f66912d143c)

## Project Summary
- This is my submission for the KrakenFlex Back End Technical Test.
- I have built the app using typescript and the Express Framework
- I have also used the package `TypeDI` to manage dependancy injection within the app
- The app is a single endpoint API that uses a repository class to manage calls to the kraken flex test api and a service class to manage the programs business logic regarding these calls

## Set up
- Install NodeJS and npm https://nodejs.org/en/download/
- run `npm install` in the root of this repository to install the Projects dependencies
- Add the Kraken Flex API Key to the project config
    - Navigate to `src/config/kraken-flex-api.json`
    - Replace `<insert api key here>` with your apikey


## Running the Program
- Execute `npm run build` to compile the program
- Execute `npm run start` to start the local server
- Use the following curl command to hit the endpoint for a successful run `curl -X POST "http://localhost:3000/outages/norwich-pear-tree?date=2022-01-01"`


## Running Unit Tests
- use the command `npm test` to run the unit tests