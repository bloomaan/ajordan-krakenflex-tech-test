import { assert } from 'chai';
import { mock, instance, when, verify, deepEqual } from 'ts-mockito';
import SiteOutageService from '../../src/services/SiteOutageService';
import KFAPIRepo from '../../src/repositories/KrakenFlexAPIRepository';
// Models
import SiteOutage from '../../src/models/SiteOutage';
import SiteInfo from '../../src/models/SiteInfo';

describe('SiteOutageService', () => {
  describe('#getAllByIdAfterDate', async () => {
    it('should filter outages by site devices and date provided', async () => {
      const dateFrom = new Date('2022-01-01');
      const siteId = 'kingfisher';
      const siteInfo: SiteInfo = {
        "id": "kingfisher",
        "name": "KingFisher",
        "devices": [
          {
            "id": "002b28fc-283c-47ec-9af2-ea287336dc1b",
            "name": "Battery 1"
          }
        ]
      };
      const expectedSiteOutage: SiteOutage = {
        "id": "002b28fc-283c-47ec-9af2-ea287336dc1b",
        "name": "Battery 1",
        "begin": "2022-05-23T12:21:27.377Z",
        "end": "2022-11-13T02:16:38.905Z"
      };

      const kfapiRepoMock = mock(KFAPIRepo);
      when(kfapiRepoMock.getAllOutages()).thenResolve(
        [
          // Begins before filtering date
          {
            "id": "002b28fc-283c-47ec-9af2-ea287336dc1b",
            "begin": "2021-07-26T17:09:31.036Z",
            "end": "2021-08-29T00:37:42.253Z"
          },
          // has wrong id
          {
            "id": "086b0d53-b311-4441-aaf3-935646f03d4d",
            "begin": "2022-05-23T12:21:27.377Z",
            "end": "2022-11-13T02:16:38.905Z"
          },
          // this should be correct
          {
            "id": "002b28fc-283c-47ec-9af2-ea287336dc1b",
            "begin": "2022-05-23T12:21:27.377Z",
            "end": "2022-11-13T02:16:38.905Z"
          }
        ]
      );
      when(kfapiRepoMock.getSiteInfoById(siteId)).thenResolve(siteInfo);

      const testSubject = new SiteOutageService(instance(kfapiRepoMock));
      const result = await testSubject.getAllByIdAfterDate(siteId, dateFrom);

      assert.deepEqual(result, [expectedSiteOutage]);
      verify(kfapiRepoMock.getSiteInfoById(siteId)).once();
      verify(kfapiRepoMock.getAllOutages()).once();
    });
  });
  describe('#save()', () => {
    it('should call repo.saveSiteOutage(siteId, SiteOutages)', async () => {
      const siteId = 'kingfisher';
      const siteOutage: SiteOutage = {
        "id": "002b28fc-283c-47ec-9af2-ea287336dc1b",
        "name": "Battery 1",
        "begin": "2022-05-23T12:21:27.377Z",
        "end": "2022-11-13T02:16:38.905Z"
      };
      const kfapiRepoMock = mock(KFAPIRepo);
      when(kfapiRepoMock.saveSiteOutage(siteId, deepEqual([siteOutage]))).thenResolve(undefined);
      const testSubject = new SiteOutageService(instance(kfapiRepoMock));
      await testSubject.save(siteId, [siteOutage]);

      verify(kfapiRepoMock.saveSiteOutage(siteId, deepEqual([siteOutage]))).once();
    });
  });
});