import { mock, instance, when, verify, deepEqual, anything } from 'ts-mockito';
import { Request, Response } from "express";
// Services
import SiteOutageService from "../../src/services/SiteOutageService";
// Contoller
import OutagesController from "../../src/controllers/OutagesController";
// Models
import SiteOutage from '../../src/models/SiteOutage';
import IncorrectInput from '../../src/repositories/exceptions/IncorrectInput';
import NotFound from '../../src/repositories/exceptions/NotFound';
import PermissionRequired from '../../src/repositories/exceptions/PermissionRequired';

describe('OutagesController',async () => {
  describe('#updateOutagesForSiteAndHandleErrors',async () => {
    it('should fetch the site outages and save them',async () => {
      const dateFrom = "2022-01-01";
      const siteId = "kingfisher";
      const siteOutages: SiteOutage[] = [
        {
          "id": "002b28fc-283c-47ec-9af2-ea287336dc1b",
          "name": "Battery 1",
          "begin": "2022-05-23T12:21:27.377Z",
          "end": "2022-11-13T02:16:38.905Z"
        },
        {
          "id": "002b28fc-283c-47ec-9af2-ea287336dc1b",
          "name": "Battery 1",
          "begin": "2022-12-04T09:59:33.628Z",
          "end": "2022-12-12T22:35:13.815Z"
        },
        {
          "id": "086b0d53-b311-4441-aaf3-935646f03d4d",
          "name": "Battery 2",
          "begin": "2022-07-12T16:31:47.254Z",
          "end": "2022-10-13T04:05:10.044Z"
        }
      ];

      const siteOutageServiceMock: SiteOutageService = mock(SiteOutageService);
      when(siteOutageServiceMock.getAllByIdAfterDate(siteId, deepEqual(new Date(dateFrom)))).thenResolve(siteOutages);
      when(siteOutageServiceMock.save(siteId, deepEqual(siteOutages))).thenResolve(undefined);

      const requestMock = mock<Request>();
      when(requestMock.query).thenReturn({"date": dateFrom});
      when(requestMock.params).thenReturn({"siteId":siteId});

      const responseMock = mock<Response>();


      const testSubject = new OutagesController(instance(siteOutageServiceMock));
      await testSubject.updateOutagesForSiteAndHandleErrors(instance(requestMock), instance(responseMock));

      verify(siteOutageServiceMock.getAllByIdAfterDate(siteId, deepEqual(new Date(dateFrom)))).once();
      verify(siteOutageServiceMock.save(siteId, deepEqual(siteOutages))).once();
      verify(responseMock.json(deepEqual({
        status: "The following data was sucessfully sent to the site-outages endpoint",
        data: siteOutages
      }))).once();
    });
    it('should fetch the siteoutages and handle a save failure',async () => {
      const dateFrom = "2022-01-01";
      const siteId = "kingfisher";
      const siteOutages: SiteOutage[] = [
        {
          "id": "002b28fc-283c-47ec-9af2-ea287336dc1b",
          "name": "Battery 1",
          "begin": "2022-05-23T12:21:27.377Z",
          "end": "2022-11-13T02:16:38.905Z"
        },
        {
          "id": "002b28fc-283c-47ec-9af2-ea287336dc1b",
          "name": "Battery 1",
          "begin": "2022-12-04T09:59:33.628Z",
          "end": "2022-12-12T22:35:13.815Z"
        },
        {
          "id": "086b0d53-b311-4441-aaf3-935646f03d4d",
          "name": "Battery 2",
          "begin": "2022-07-12T16:31:47.254Z",
          "end": "2022-10-13T04:05:10.044Z"
        }
      ];

      const siteOutageServiceMock: SiteOutageService = mock(SiteOutageService);
      when(siteOutageServiceMock.getAllByIdAfterDate(siteId, deepEqual(new Date(dateFrom)))).thenResolve(siteOutages);
      when(siteOutageServiceMock.save(siteId, deepEqual(siteOutages))).thenReject(new IncorrectInput('test'));

      const requestMock = mock<Request>();
      when(requestMock.query).thenReturn({"date": dateFrom});
      when(requestMock.params).thenReturn({"siteId":siteId});

      const responseMock = mock<Response>();


      const testSubject = new OutagesController(instance(siteOutageServiceMock));
      await testSubject.updateOutagesForSiteAndHandleErrors(instance(requestMock), instance(responseMock));


      verify(siteOutageServiceMock.getAllByIdAfterDate(siteId, deepEqual(new Date(dateFrom)))).once();
      verify(siteOutageServiceMock.save(siteId, deepEqual(siteOutages))).once();
      verify(responseMock.status(500)).once();
      verify(responseMock.send('test')).once();
    });
    it('should handle failure if the site id results in a not found when fetching site outages',async () => {
      const dateFrom = "2022-01-01";
      const siteId = "kingfisher";

      const siteOutageServiceMock: SiteOutageService = mock(SiteOutageService);
      when(siteOutageServiceMock.getAllByIdAfterDate(siteId, deepEqual(new Date(dateFrom)))).thenReject(new NotFound('test'));

      const requestMock = mock<Request>();
      when(requestMock.query).thenReturn({"date": dateFrom});
      when(requestMock.params).thenReturn({"siteId":siteId});

      const responseMock = mock<Response>();


      const testSubject = new OutagesController(instance(siteOutageServiceMock));
      await testSubject.updateOutagesForSiteAndHandleErrors(instance(requestMock), instance(responseMock));

      verify(siteOutageServiceMock.getAllByIdAfterDate(siteId, deepEqual(new Date(dateFrom)))).once();
      verify(siteOutageServiceMock.save(anything(), anything())).never();
      verify(responseMock.status(404)).once();
      verify(responseMock.send("NotFound: test")).once();
    });
    it('should return the status 500 if we fail to fetch the site outages',async () => {
      const dateFrom = "2022-01-01";
      const siteId = "kingfisher";

      const siteOutageServiceMock: SiteOutageService = mock(SiteOutageService);
      when(siteOutageServiceMock.getAllByIdAfterDate(siteId, deepEqual(new Date(dateFrom)))).thenReject(new PermissionRequired('test'));

      const requestMock = mock<Request>();
      when(requestMock.query).thenReturn({"date": dateFrom});
      when(requestMock.params).thenReturn({"siteId":siteId});

      const responseMock = mock<Response>();


      const testSubject = new OutagesController(instance(siteOutageServiceMock));
      await testSubject.updateOutagesForSiteAndHandleErrors(instance(requestMock), instance(responseMock));

      verify(siteOutageServiceMock.getAllByIdAfterDate(siteId, deepEqual(new Date(dateFrom)))).once();
      verify(siteOutageServiceMock.save(anything(), anything())).never();
      verify(responseMock.status(500)).once();
      verify(responseMock.send("test")).once();
    });
    it('should return the status 400 if site id is missing',async () => {
      const dateFrom = "2022-01-01";

      const siteOutageServiceMock: SiteOutageService = mock(SiteOutageService);

      const requestMock = mock<Request>();
      when(requestMock.query).thenReturn({"date": dateFrom});
      when(requestMock.params).thenReturn({});

      const responseMock = mock<Response>();


      const testSubject = new OutagesController(instance(siteOutageServiceMock));
      await testSubject.updateOutagesForSiteAndHandleErrors(instance(requestMock), instance(responseMock));

      verify(siteOutageServiceMock.getAllByIdAfterDate(anything(), anything())).never();
      verify(siteOutageServiceMock.save(anything(), anything())).never();
      verify(responseMock.status(400)).once();
      verify(responseMock.send(anything())).once();
    });
    it('should return the status 400 if a valid date is not supplied in the date query param',async () => {
      const dateFrom = "not a date";

      const siteOutageServiceMock: SiteOutageService = mock(SiteOutageService);

      const requestMock = mock<Request>();
      when(requestMock.query).thenReturn({"date": dateFrom});

      const responseMock = mock<Response>();


      const testSubject = new OutagesController(instance(siteOutageServiceMock));
      await testSubject.updateOutagesForSiteAndHandleErrors(instance(requestMock), instance(responseMock));

      verify(siteOutageServiceMock.getAllByIdAfterDate(anything(), anything())).never();
      verify(siteOutageServiceMock.save(anything(), anything())).never();
      verify(responseMock.status(400)).once();
      verify(responseMock.send(anything())).once();
    });
    it('should return the status 400 if the date query param is missing',async () => {
      const siteOutageServiceMock: SiteOutageService = mock(SiteOutageService);

      const requestMock = mock<Request>();
      when(requestMock.query).thenReturn({});

      const responseMock = mock<Response>();


      const testSubject = new OutagesController(instance(siteOutageServiceMock));
      await testSubject.updateOutagesForSiteAndHandleErrors(instance(requestMock), instance(responseMock));

      verify(siteOutageServiceMock.getAllByIdAfterDate(anything(), anything())).never();
      verify(siteOutageServiceMock.save(anything(), anything())).never();
      verify(responseMock.status(400)).once();
      verify(responseMock.send(anything())).once();
    });
  });
});