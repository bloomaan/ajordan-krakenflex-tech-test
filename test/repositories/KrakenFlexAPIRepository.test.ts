import { assert } from 'chai';
import { mock, instance, when, verify, deepEqual } from 'ts-mockito';
import { AxiosError, AxiosInstance, AxiosResponse} from "axios";
import KFAPIRepo from '../../src/repositories/KrakenFlexAPIRepository';
import Outage from '../../src/models/Outage';
import PermissionRequired from '../../src/repositories/exceptions/PermissionRequired';
import RateLimit from '../../src/repositories/exceptions/RateLimit';
import SiteInfo from '../../src/models/SiteInfo';
import NotFound from '../../src/repositories/exceptions/NotFound';
import SiteOutage from '../../src/models/SiteOutage';
import IncorrectInput from '../../src/repositories/exceptions/IncorrectInput';

describe('KrakenFlexAPIRepository', () => {
  describe('#getAllOutages', () => {
    it('should call GET /outages on the client and return the response data', async () => {
      const expectedOutage: Outage = {
        "id": "002b28fc-283c-47ec-9af2-ea287336dc1b",
        "begin": "2022-05-23T12:21:27.377Z",
        "end": "2022-11-13T02:16:38.905Z"
      };
      const dummyResponse: AxiosResponse = {data:[expectedOutage],status:200, statusText: 'ok', headers:{}, config:{}};

      const clientMock: AxiosInstance = mock<AxiosInstance>();
      when(clientMock.get('/outages')).thenResolve(dummyResponse);

      const testSubject = new KFAPIRepo(instance(clientMock));
      const result = await testSubject.getAllOutages();

      assert.deepEqual(result, [expectedOutage]);
      verify(clientMock.get('/outages')).once();
    });

    it('should call GET /outages on the client and handle a 403 error thrown by the client', async () => {
      const dummy403Response: AxiosResponse = {data:{},status:403, statusText: 'Not Found', headers:{}, config:{}};
      const clientMock: AxiosInstance = mock<AxiosInstance>();
      when(clientMock.get('/outages')).thenReject(new AxiosError('403 test', '403',{}, {}, dummy403Response));

      const testSubject = new KFAPIRepo(instance(clientMock));
      try {
        await testSubject.getAllOutages();
      } catch (e) {
        assert.instanceOf(e, PermissionRequired);
      }
    });

    it('should call GET /outages on the client and handle a 429 error thrown by the client', async () => {
      const dummy429Response: AxiosResponse = {data:{},status:429, statusText: 'Rate Limit', headers:{}, config:{}};
      const clientMock: AxiosInstance = mock<AxiosInstance>();
      when(clientMock.get('/outages')).thenReject(new AxiosError('429 test', '429',{}, {}, dummy429Response));

      const testSubject = new KFAPIRepo(instance(clientMock));
      try {
        await testSubject.getAllOutages();
      } catch (e) {
        assert.instanceOf(e, RateLimit);
      }
    });

    it('should call GET /outages on the client and handle a 500 (Other Axios Error) error thrown by the client', async () => {
      const dummy500Response: AxiosResponse = {data:{},status:500, statusText: 'Rate Limit', headers:{}, config:{}};
      const clientMock: AxiosInstance = mock<AxiosInstance>();
      when(clientMock.get('/outages')).thenReject(new AxiosError('500 test', '500',{}, {}, dummy500Response));

      const testSubject = new KFAPIRepo(instance(clientMock));
      try {
        await testSubject.getAllOutages();
      } catch (e) {
        assert.instanceOf(e, AxiosError);
      }
    });

    it('should call GET /outages on the client and handle a generic error thrown by the client', async () => {
      const genericError: Error = new Error('Generic Error');
      const clientMock: AxiosInstance = mock<AxiosInstance>();
      when(clientMock.get('/outages')).thenReject(genericError);

      const testSubject = new KFAPIRepo(instance(clientMock));
      try {
        await testSubject.getAllOutages();
      } catch (e) {
        assert.deepEqual(e, genericError);
      }
    });
  });

  describe('#getSiteInfoById', () => {
    it('should call GET /site-info/{siteId} and return the response data', async () => {
      const unsafeSiteId = '&kingfisher=';
      const safeSiteId = encodeURIComponent(unsafeSiteId);
      const expectedSiteInfo: SiteInfo = {
        "id": "kingfisher",
        "name": "KingFisher",
        "devices": [
          {
            "id": "002b28fc-283c-47ec-9af2-ea287336dc1b",
            "name": "Battery 1"
          },
          {
            "id": "086b0d53-b311-4441-aaf3-935646f03d4d",
            "name": "Battery 2"
          }
        ]
      };
      const dummyResponse: AxiosResponse = {data:expectedSiteInfo,status:200, statusText: 'ok', headers:{}, config:{}};
      const clientMock: AxiosInstance = mock<AxiosInstance>();
      when(clientMock.get(`/site-info/${safeSiteId}`)).thenResolve(dummyResponse);

      const testSubject = new KFAPIRepo(instance(clientMock));
      const result = await testSubject.getSiteInfoById(unsafeSiteId);

      assert.notEqual(unsafeSiteId, safeSiteId);
      assert.deepEqual(result, expectedSiteInfo);
      verify(clientMock.get(`/site-info/${safeSiteId}`)).once();
    });

    it('should call GET /site-info/{siteId} and handle a 404 status', async () => {
      const siteId = 'unknown-id';
      const dummyResponse: AxiosResponse = {data:{},status:404, statusText: 'Not Found', headers:{}, config:{}};
      const clientMock: AxiosInstance = mock<AxiosInstance>();
      when(clientMock.get(`/site-info/${siteId}`)).thenReject(new AxiosError('Not Found', '404', {}, {}, dummyResponse));

      const testSubject = new KFAPIRepo(instance(clientMock));
      try {
        await testSubject.getSiteInfoById(siteId);
      } catch (e) {
        assert.instanceOf(e, NotFound);
      }

      verify(clientMock.get(`/site-info/${siteId}`)).once();
    });
  });

  describe('#saveSiteOutage', () => {
    it('should call POST /site-outage/{siteId} with the siteId and array of outages provided sucessfully', async () => {
      const unsafeSiteId = '&kingfisher=';
      const safeSiteId = encodeURIComponent(unsafeSiteId);
      const siteOutage: SiteOutage = {
        "id": "002b28fc-283c-47ec-9af2-ea287336dc1b",
        "name": "Battery 1",
        "begin": "2022-05-23T12:21:27.377Z",
        "end": "2022-11-13T02:16:38.905Z"
      };
      const dummyResponse: AxiosResponse = {data:{},status:200, statusText: 'ok', headers:{}, config:{}};
      const clientMock: AxiosInstance = mock<AxiosInstance>();
      when(clientMock.post(`/site-outages/${safeSiteId}`, deepEqual([siteOutage]))).thenResolve(dummyResponse);

      const testSubject = new KFAPIRepo(instance(clientMock));
      await testSubject.saveSiteOutage(unsafeSiteId, [siteOutage]);

      assert.notEqual(unsafeSiteId, safeSiteId);
      verify(clientMock.post(`/site-outages/${safeSiteId}`, deepEqual([siteOutage]))).once();
    });

    it('should call POST /site-outage/{siteId} with the siteId and array of outages provided and handle 404 status', async () => {
      const siteId = 'unknown-id';
      const siteOutage: SiteOutage = {
        "id": "002b28fc-283c-47ec-9af2-ea287336dc1b",
        "name": "Battery 1",
        "begin": "2022-05-23T12:21:27.377Z",
        "end": "2022-11-13T02:16:38.905Z"
      };
      const dummyResponse: AxiosResponse = {data:{},status:404, statusText: 'Not Found', headers:{}, config:{}};
      const clientMock: AxiosInstance = mock<AxiosInstance>();
      when(clientMock.post(`/site-outages/${siteId}`, deepEqual([siteOutage]))).thenReject(new AxiosError('Not Found', '404', {}, {}, dummyResponse));

      const testSubject = new KFAPIRepo(instance(clientMock));
      try {
        await testSubject.saveSiteOutage(siteId, [siteOutage]);
      } catch (e) {
        assert.instanceOf(e, NotFound);
      }

      verify(clientMock.post(`/site-outages/${siteId}`, deepEqual([siteOutage]))).once();
    });

    it('should call POST /site-outage/{siteId} with the siteId and array of outages provided and handle 400 status', async () => {
      const siteId = 'unknown-id';
      const siteOutage: SiteOutage = {
        "id": "002b28fc-283c-47ec-9af2-ea287336dc1b",
        "name": "Battery 1",
        "begin": "2022-05-23T12:21:27.377Z",
        "end": "2022-11-13T02:16:38.905Z"
      };
      const dummyResponse: AxiosResponse = {data:{},status:400, statusText: 'Wrong Data', headers:{}, config:{}};
      const clientMock: AxiosInstance = mock<AxiosInstance>();
      when(clientMock.post(`/site-outages/${siteId}`, deepEqual([siteOutage]))).thenReject(new AxiosError('Not Found', '404', {}, {}, dummyResponse));

      const testSubject = new KFAPIRepo(instance(clientMock));
      try {
        await testSubject.saveSiteOutage(siteId, [siteOutage]);
      } catch (e) {
        assert.instanceOf(e, IncorrectInput);
      }

      verify(clientMock.post(`/site-outages/${siteId}`, deepEqual([siteOutage]))).once();
    });
  });
});